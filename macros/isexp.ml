[@@@warning "-33-26"]
open Ppxlib
open Asttypes
open Parsetree
open Ast_helper
open Ppx_deriving.Ast_convenience

let deriver = "isexp"
let raise_errorf = Ppx_deriving.raise_errorf

let attr_from_sexp attrs =
  Ppx_deriving.(attrs |> attr ~deriver "from_sexp" |> Arg.(get_attr ~deriver expr))

let attr_default attrs =
  Ppx_deriving.(attrs |> attr ~deriver "default" |> Arg.(get_attr ~deriver expr))


let convert_name str =
  let is_uppercase c = Stdlib.(Char.lowercase_ascii c <> c) in
  let count pred str =
    let count = ref 0 in
    Stdlib.String.iter (fun c -> if pred c then incr count) str;
    !count in
  let un_snake_case str =
    Stdlib.String.map (function '_' -> '-' | c -> c) str
    |> Stdlib.String.lowercase_ascii in
  let un_camel_case str =
    let init = ref true in
    let is_init () = if !init then (init := false; false) else true in
    Stdlib.String.to_seq str
    |> Stdlib.Seq.flat_map (fun c ->
      let lc = Stdlib.Char.lowercase_ascii c in
      if is_uppercase c && is_init ()
      then Stdlib.Seq.(cons '-' (cons lc empty))
      else Stdlib.Seq.return lc
    ) |> Stdlib.String.of_seq in
  if count (Char.equal '_') str > 0
  then un_snake_case str
  else if count is_uppercase str > 1
  then un_camel_case str
  else Stdlib.String.lowercase_ascii str

let rec expr_of_typ (quoter: Ppx_deriving.quoter) (typ: core_type) =
    let expr_of_type = expr_of_typ quoter in
    let loc = typ.ptyp_loc in
    match attr_from_sexp typ.ptyp_attributes with
    | Some fn -> Ppx_deriving.quote ~quoter fn
    | None ->
      match typ with
      | {ptyp_desc=Ptyp_any; _} -> raise_errorf ~loc "use of unsupported type _"
      | {ptyp_desc=Ptyp_var var; _} -> Exp.ident {txt=Lident ("poly_" ^ var); loc}
      | {ptyp_desc=Ptyp_arrow (_, _, _) ; _} -> raise_errorf ~loc "use of unsupported type construct arrow"
      | [%type: [%t? _] option] -> raise_errorf ~loc "unsupported use of optional"
      | {ptyp_desc=Ptyp_tuple args ; ptyp_loc=loc; _} ->
        let args = List.mapi (fun i ty -> ("___arg" ^ Int.to_string i), ty) args in
        let arg_pat =
          let arg_pat_variables = List.map (fun (name, _) -> pvar name) args in
          List.fold_right (fun hd tl -> [%pat? [%p hd] :: [%p tl]]) arg_pat_variables [%pat? []] in
        let tuple_body =
          Exp.tuple (List.map (fun (name, ty) ->
            let fn = expr_of_typ quoter ty in
            let arg = Exp.ident {txt=Lident name; loc} in
            [%expr [%e fn] [%e arg]]
          ) args) in
        let fn = Exp.function_ [
          Exp.case arg_pat tuple_body;
          Exp.case [%pat? ls]
            [%expr raise
                     (Failure
                        ("unexpected arguments to tuple " ^
                         [%e Exp.constant (
                           Pconst_string (
                             Format.asprintf "%a" Pprintast.core_type typ,
                             loc,
                             None
                           ))] ^ ": " ^
                         (Stdlib.String.concat ";"
                            (Stdlib.List.map (fun sexp -> Sexplib.Sexp.With_layout.Forget.t sexp |> Sexplib.Sexp.to_string_hum)
                               ls)) ))]
        ] in
        [%expr fun ls -> [%e fn] (match ls with
          | Sexplib.Sexp.With_layout.List (_, ls, _) ->
            List.filter_map (function Sexplib.Sexp.With_layout.Sexp s -> Some s | _ -> None) ls
          | ls -> [ls]
        ) ]
      | [%type: [%t? arg] with_pos]  ->
        let fn = expr_of_typ quoter arg in
        let fn = Exp.function_ [
          Exp.case [%pat? Sexplib.Sexp.With_layout.List (st, _, ed) as elt]
            [%expr ((st, ed), [%e fn] elt)];
          Exp.case [%pat? Sexplib.Sexp.With_layout.Atom (st, _, _) as elt]
            [%expr ((st, st), [%e fn] elt)];
        ] in
        [%expr fun ls -> [%e fn] ls ]
      | [%type: [%t? arg] list] ->
        let fn = expr_of_typ quoter arg in
        let fn = Exp.function_ [
          Exp.case [%pat? Sexplib.Sexp.With_layout.Sexp s] [%expr Some ([%e fn] s)];
          Exp.case [%pat? _] [%expr None]
        ] in
        [%expr function
          | Sexplib.Sexp.With_layout.List (_, ls, _) ->
            Stdlib.List.filter_map [%e fn] ls
        ]
      | [%type: [%t? arg] array] ->
        let fn = expr_of_typ quoter arg in
        let fn = Exp.function_ [
          Exp.case [%pat? Sexplib.Sexp.With_layout.Sexp s] [%expr Some ([%e fn] s)];
          Exp.case [%pat? _] [%expr None]
        ] in
        [%expr function
          | Sexplib.Sexp.With_layout.List (_, ls, _) ->
            Stdlib.Array.of_list (Stdlib.List.filter_map [%e fn] ls)
        ]
      | {ptyp_desc=Ptyp_constr (lid, args) ; _} ->
        let name = {lid with txt = Ppx_deriving.mangle_lid (`Suffix "from_sexp") lid.txt} in
        let fn = Ast_helper.Exp.ident name in
        begin match args with
        | [] -> fn
        | _ -> Ast_helper.Exp.apply fn (List.map (fun arg -> Nolabel, expr_of_typ quoter arg) args)
        end
      | {ptyp_desc=Ptyp_object (_, _) ; _} -> raise_errorf ~loc "use of unsupported type construct object"
      | {ptyp_desc=Ptyp_class (_, _) ; _} -> raise_errorf ~loc "use of unsupported type construct class"
      | {ptyp_desc=Ptyp_alias (_, _) ; _} -> raise_errorf ~loc "use of unsupported type construct alias"
      | {ptyp_desc=Ptyp_variant (_, _, _) ; _} -> raise_errorf ~loc "use of unsupported type construct variant"
      | {ptyp_desc=Ptyp_poly (_, _)  ; _} -> raise_errorf ~loc "use of unsupported type construct poly"
      | {ptyp_desc=Ptyp_package _ ; _} -> raise_errorf ~loc "use of unsupported type construct package"
      | {ptyp_desc=Ptyp_extension _; _} -> raise_errorf ~loc "use of unsupported type construct extension"
and sig_of_type ~options ~path type_decl : signature_item list =
  [Sig.value (Val.mk (mknoloc (Ppx_deriving.mangle_type_decl (`Suffix "from_sexp") type_decl))
                (core_type_of_decl ~options ~path type_decl))]
and core_type_of_decl ~options:_ ~path:_ (type_decl: type_declaration) : core_type =
  let loc = type_decl.ptype_loc in
  let typ = Ppx_deriving.core_type_of_type_decl type_decl in
  Ppx_deriving.poly_arrow_of_type_decl (fun var -> [%type: Sexplib.Sexp.With_layout.t -> [%t var]])
    type_decl
    [%type: Sexplib.Sexp.With_layout.t -> [%t typ]] 
and str_of_type ~options ~path ({ptype_loc=loc; _} as type_decl: type_declaration) : value_binding list =
  let quoter = Ppx_deriving.create_quoter () in
  let operation = 
    match type_decl.ptype_kind, type_decl.ptype_manifest with
    | Ptype_abstract, Some manifest -> expr_of_typ quoter manifest
    | Ptype_record labels, _ ->
      build_matcher ~loc quoter labels
    | Ptype_variant constructors, _ -> build_constructor_matcher ~loc quoter constructors
    | _ -> failwith "epicarooney"
  in
  let polymorphize = Ppx_deriving.poly_fun_of_type_decl type_decl in
  let eta_expand expr = match expr with
      {pexp_desc=Pexp_fun _; _ } -> expr
    | _ -> [%expr fun x -> [%e expr] x ] in
  let out_type = Ppx_deriving.strong_type_of_type @@ core_type_of_decl ~options ~path type_decl in
  let eq_var = pvar (Ppx_deriving.mangle_type_decl (`Suffix "from_sexp") type_decl) in
  [Vb.mk ~attrs:[Ppx_deriving.attr_warning [%expr "-39"]]
     (Pat.constraint_ eq_var out_type)
     (Ppx_deriving.sanitize ~quoter (eta_expand (polymorphize operation)))
  ]
and build_matcher ?(wrap_extractor=fun x -> x) quoter ~loc ls =
  let field_name field = field.pld_name.txt in
  let field_default field =
    attr_default field.pld_attributes
    |> Option.map (Ppx_deriving.quote ~quoter) in 
  let field_result_label field =
    Ppx_deriving.mangle_lid
                        (`Suffix "result")
                        (Lident field.pld_name.txt) in
  let result_vars =
    List.map (fun field ->
      Vb.mk (pvar @@ Longident.name @@ field_result_label field) [%expr ref None]
    ) ls in
  let result_vars =
    List.map (fun field ->
      Vb.mk (pvar @@ Longident.name @@ field_result_label field) [%expr ref None]
    ) ls in
  let is_optional field = match field.pld_type with
    | [%type: [%t? ty ] option] -> Some ty
    | _ -> None in
  let extractor =
    wrap_extractor @@
    Exp.record 
      (List.map (fun field ->
         match is_optional field, field_default field with
         | None, None -> 
           {txt=Lident (field_name field);loc=field.pld_loc},
           [%expr match ![%e Exp.ident {txt=field_result_label field; loc=field.pld_loc}] with
             | Some result -> result
             | None ->
               raise (Failure ("field " ^
                               [%e Exp.constant (Pconst_string (convert_name @@ field_name field, loc, None))] ^
                               " not found in " ^
                               (Stdlib.String.concat ";"
                                  (Stdlib.List.map (fun sexp -> Sexplib.Sexp.With_layout.Forget.t sexp |> Sexplib.Sexp.to_string_hum)
                                  ls))
                              ))
           ]
         | _, Some default ->
           {txt=Lident (field_name field);loc=field.pld_loc},
           [%expr match ![%e Exp.ident {txt=field_result_label field; loc=field.pld_loc}] with
             | Some result -> result
             | None -> [%e default ]
           ]
         | Some _, _ ->
           {txt=Lident (field_name field);loc=field.pld_loc},
           [%expr ![%e Exp.ident {txt=field_result_label field; loc=field.pld_loc}]]
       ) ls) None in
  let matcher =
    List.map (fun field ->
      let field_name = field_name field in
      let ty = match is_optional field with
        | None -> field.pld_type
        | Some underlying_ty -> underlying_ty in
      let matcher = expr_of_typ quoter ty in
      Ast_helper.Exp.case
        [%pat? Sexplib.Sexp.With_layout.List (
          st,
          Sexplib.Sexp.With_layout.Sexp
            (Sexplib.Sexp.With_layout.Atom (_, [%p Ast_helper.Pat.constant (Pconst_string (convert_name @@ field_name, loc, None))], _)) :: rest,
          ed
        )] ([%expr
               [%e Ast_helper.Exp.ident {txt=(field_result_label field); loc}] :=
                 Some ([%e matcher] (Sexplib.Sexp.With_layout.List (st, rest, ed)))
             ])
    ) ls @ [
      Ast_helper.Exp.case [%pat? _] [%expr ()]
    ] in
  Ast_helper.Exp.let_ Nonrecursive result_vars
    [%expr function
      | Sexplib.Sexp.With_layout.List (_, ls, _) -> begin
          let ls = Stdlib.List.filter_map (function Sexplib.Sexp.With_layout.Sexp s -> Some s | _ -> None) ls in
          Stdlib.List.iter [%e (Ast_helper.Exp.function_ matcher)] ls;
          [%e extractor]
      end
      | _ -> raise (Failure "unexpected sexp to record")
    ]
and build_constructor_matcher quoter : loc:location -> constructor_declaration list -> expression =
  fun ~loc ls -> 
  let matcher =
    List.map (fun (field: constructor_declaration) ->
      let field_name = field.pcd_name.txt |> String.lowercase_ascii in
      let sexp_field_name = field.pcd_name.txt |> convert_name in
      let arg_name i = Format.sprintf "__arg%d" i in
      match field.pcd_args with
      | Pcstr_tuple tys ->
        let tys_pat = List.mapi (fun i _ -> pvar (arg_name i)) tys
                      |> List.fold_right (fun pat rest -> [%pat? [%p pat] :: [%p rest]]) in
        let tys_exp = Exp.construct {txt=Lident field.pcd_name.txt; loc}
                        (match tys with
                           [] -> None
                         | _ -> Some (Exp.tuple @@ List.mapi (fun i ty ->
                           Exp.apply (expr_of_typ quoter ty) [Nolabel, Exp.ident {txt=Lident (arg_name i); loc}]
                         ) tys)) in
        let args_matcher = 
          [%expr fun ls ->
            match Stdlib.List.filter_map (function Sexplib.Sexp.With_layout.Sexp s -> Some s | _ -> None) ls with
            | [%p tys_pat [%pat? [] ] ] -> [%e tys_exp ]
            | _ ->
              raise (Failure ("unexpected arguments to constructor " ^
                              [%e Exp.constant (Pconst_string (sexp_field_name, loc, None))]))
          ] in
        Ast_helper.Exp.case
          [%pat? Sexplib.Sexp.With_layout.List (
            _,
            Sexplib.Sexp.With_layout.Sexp
              (Sexplib.Sexp.With_layout.Atom (_, [%p Ast_helper.Pat.constant (Pconst_string (sexp_field_name, loc, None))], _)) :: rest,
            _
          )] ([%expr [%e args_matcher] rest])
      | Pcstr_record fields ->
        let args_matcher = 
          build_matcher ~wrap_extractor:(fun record -> Exp.construct {txt=Lident field.pcd_name.txt; loc} (Some record))
            quoter ~loc fields in
        Ast_helper.Exp.case
          [%pat? Sexplib.Sexp.With_layout.List (
            st,
            Sexplib.Sexp.With_layout.Sexp
              (Sexplib.Sexp.With_layout.Atom (_, [%p Ast_helper.Pat.constant (Pconst_string (sexp_field_name, loc, None))], _)) :: rest,
            ed
          )] ([%expr [%e args_matcher] (Sexplib.Sexp.With_layout.List (st, rest, ed))])
    ) ls @ [
      Ast_helper.Exp.case [%pat? _] [%expr raise (Failure ("from_sexp: unexpected sexp"))]
    ] in
  Exp.function_ matcher


let () =
  Ppx_deriving.(register (create deriver
                            ~core_type: (Ppx_deriving.with_quoter expr_of_typ)
                            ~type_decl_str:(fun ~options ~path type_decls ->
                              [Str.value Recursive (List.concat_map (str_of_type ~options ~path) type_decls)]
                            )
                            ~type_decl_sig:(fun ~options ~path type_decls ->
                              List.concat_map (sig_of_type ~options ~path) type_decls
                            )
                            ()
                         ))

