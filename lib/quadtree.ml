open Core

let filter_mappend l ~f acc = List.rev_append (List.rev_filter_map l ~f) acc

let remove_first ~f ls =
  let rec loop f acc = function
    | [] -> None
    | h :: t ->
      if f h
      then Some (List.rev_append acc t, h)
      else loop f (h :: acc) t in
  loop f [] ls
  
let remove_first_map ~f ls =
  let rec loop f acc = function
    | [] -> None
    | h :: t ->
      match f h with
      | Some h -> Some (List.rev_append acc t, h)
      | None -> loop f (h :: acc) t in
  loop f [] ls

type point = float * float
type dim = {w:float; h: float}
type rect = point * dim

let subdivide {w;h} = {w=w/.2.; h=h/.2.}

let midpoint ((x,y), dim) =
  let dim = subdivide dim in
  (x +. dim.w,y +. dim.h)

let nw ((x,y), dim) =
  let dim = subdivide dim in
  (x,y), dim

let ne ((x,y), dim) =
  let dim = subdivide dim in
  (x +. dim.w,y), dim

let se ((x,y), dim) =
  let dim = subdivide dim in
  (x +. dim.w,y +. dim.h), dim

let sw ((x,y), dim) =
  let dim = subdivide dim in
  (x,y +. dim.h), dim

let categorise ((x,y), {w;h}) ((px,py), {w=pw;h=ph}) =
  match Float.(
    x  +. w/.2.  <= px      ,
    px +. pw    <= x +. w/.2.,
    py         <= y +. h/.2.,
    y +. h/.2.   <= py +. ph
  ) with
  | true, _, _, false -> `NE
  | _, true, _, false -> `NW
  | true, _, false, _ -> `SE
  | _, true, false, _ -> `SW
  | _ -> `Overlaps

let%test "check categorise of basic 1" =
  Poly.equal (categorise ((0.,0.),{w=10.0;h=10.0}) ((0.,0.),{w=2.0;h=2.0})) `NW

let%test "check categorise of basic 2" =
  Poly.equal (categorise ((0.,0.),{w=10.0;h=10.0}) ((0.,0.),{w=5.0;h=5.0})) `Overlaps

let%test "check categorise of basic 3" =
  Poly.equal (categorise ((0.,0.),{w=10.0;h=10.0}) ((0.,0.),{w=4.99;h=4.99})) `NW

let%test "check categorise of basic 4" =
  Poly.equal (categorise ((0.,0.),{w=10.0;h=10.0}) ((1.,1.),{w=3.5;h=3.5})) `NW

let%test "check categorise of basic 5" =
  Poly.equal (categorise ((0.,0.),{w=100.0;h=100.0}) ((1.,51.),{w=3.5;h=3.5})) `SW

let%test "check categorise of basic 6" =
  Poly.equal (categorise ((0.,0.),{w=100.0;h=100.0}) ((51.,51.),{w=3.5;h=3.5})) `SE

let categorise_point ((x,y), {w;h}) (px,py) =
  match Float.(x  +. w/.2.  <= px, py <= y +. h/.2.) with
  | true, false -> `SE
  | true, true  -> `NE
  | false,false -> `SW
  | false,true  -> `NW

let points ((x,y),{w;h}) = [
  x    ,     y;
  x +. w,     y;
  x    , y +. h;
  x +. w, y +. h
]

let intersects_point ((x,y),{w;h}) (px,py) =
  Float.(
    x <= px && px <= x +. w &&
    y <= py && py <= y +. h
  )

let intersects ((x1,y1),{w=w1;h=h1}) ((x2,y2),{w=w2;h=h2}) =
  not Float.(x1+.w1<x2 || x2+.w2<x1 || y1+.h1<y2 || y2+.h2<y1)

let contains ((x1,y1),{w=w1;h=h1}) ((x2,y2),{w=w2;h=h2}) =
  Float.(x1 <= x2 && x2 +. w2 <= x1 +. w1 &&
  y1 <= y2 && y2 +. h2 <= y1 +. h1)

module Fixed = struct

  let max_depth = 10

  type +!'a node =
    | Leaf of ('a * rect) list
    | Quad of {
        nw: 'a node; ne: 'a node;
        sw: 'a node; se: 'a node;
        elts: ('a * rect) list;
      }

  type +!'a t = {
    dim: dim;
    tree: 'a node;
  }

  let rec insert_tree depth tree_bbox ((_,bbox) as node) = function
    | Leaf elts ->
      begin match categorise tree_bbox bbox with
      | (`NE | `NW | `SE | `SW) when depth < max_depth ->
        let nw' = ref [] and ne' = ref []
        and sw' = ref [] and se' = ref []
        and overlaps' = ref [] in
        List.iter (node :: elts) ~f:(fun ((_,bbox) as node) ->
          match categorise tree_bbox bbox with
          | `NE -> ne' := node :: !ne' 
          | `NW -> nw' := node :: !nw'
          | `SE -> se' := node :: !se'
          | `SW -> sw' := node :: !sw'
          | `Overlaps -> overlaps' := node :: !overlaps'
        );
        let depth = depth + 1 in
        let nw =
          let tree_bbox = nw tree_bbox in
          List.fold_left ~init:(Leaf []) ~f:(fun acc vl -> insert_tree depth tree_bbox vl acc) !nw' in
        let ne =
          let tree_bbox = ne tree_bbox in
          List.fold_left ~init:(Leaf []) ~f:(fun acc vl -> insert_tree depth tree_bbox vl acc) !ne' in
        let se =
          let tree_bbox = se tree_bbox in
          List.fold_left ~init:(Leaf []) ~f:(fun acc vl -> insert_tree depth tree_bbox vl acc) !se' in
        let sw =
          let tree_bbox = sw tree_bbox in
          List.fold_left ~init:(Leaf []) ~f:(fun acc vl -> insert_tree depth tree_bbox vl acc) !sw' in
        Quad {nw;ne;sw;se; elts= !overlaps'}
      | _ -> Leaf (node :: elts)
      end
    | Quad ({nw=nw';ne=ne';sw=sw';se=se';elts} as quad) ->
      begin match categorise tree_bbox bbox with
      | `NE ->  Quad {quad with ne=insert_tree (depth + 1) (ne tree_bbox) node ne'}
      | `NW -> Quad {quad with nw=insert_tree (depth + 1) (nw tree_bbox) node nw'}
      | `SE -> Quad {quad with se=insert_tree (depth + 1) (se tree_bbox) node se'}
      | `SW -> Quad {quad with sw=insert_tree (depth + 1) (sw tree_bbox) node sw'}
      | `Overlaps -> Quad {quad with elts = node :: elts}
      end

  let create dim = {dim; tree=Leaf []}

  let add (node, bbox) {dim;tree} =
    let tree_box = (0.,0.), dim in
    (* assert (contains tree_box bbox); *)
    let tree = insert_tree 0 tree_box (node,bbox) tree in
    {dim;tree}

  let of_list dim ls =
    List.fold_left ~init:(create dim) ~f:(fun acc node -> add node acc) ls

  let rec tree_to_list tree acc = match tree with
    | Leaf elts -> List.append acc elts
    | Quad {nw; sw; se; ne; elts} ->
      List.append acc elts
      |> tree_to_list nw
      |> tree_to_list ne
      |> tree_to_list se
      |> tree_to_list sw

  let to_list {tree; _} = tree_to_list tree []

  let rec bounding_boxes_tree tree tree_box acc = match tree with
    | Leaf _ -> tree_box :: acc
    | Quad ({nw=nw';ne=ne';sw=sw';se=se';_}) ->
      (tree_box :: acc)
      |> bounding_boxes_tree nw' (nw tree_box) 
      |> bounding_boxes_tree ne' (ne tree_box) 
      |> bounding_boxes_tree se' (se tree_box) 
      |> bounding_boxes_tree sw' (sw tree_box) 

  let bounding_boxes {dim;tree} =
    let tree_box = (0.,0.), dim in
    bounding_boxes_tree tree tree_box []

  let rec find_all_tree acc tree_box point = function
    | Leaf elts ->
      filter_mappend ~f:(fun (node, rect) -> if intersects_point rect point then Some node else None) elts acc
    | Quad ({nw=nw';ne=ne';sw=sw';se=se';elts}) ->
      let acc = filter_mappend ~f:(fun (node, rect) -> if intersects_point rect point then Some node else None) elts acc in
      match categorise_point tree_box point with
      | `SE -> find_all_tree acc (se tree_box) point se'
      | `NE -> find_all_tree acc (ne tree_box) point ne'
      | `SW -> find_all_tree acc (sw tree_box) point sw'
      | `NW -> find_all_tree acc (nw tree_box) point nw'

  let find_all {dim;tree} point =
    let tree_box = (0.,0.), dim in
    find_all_tree [] tree_box point tree

  let rec find_all_iter_tree f tree_box point = function
    | Leaf elts ->
      List.iter ~f:(fun ((_, rect) as node) -> if intersects_point rect point then f node) elts
    | Quad ({nw=nw';ne=ne';sw=sw';se=se';elts}) ->
      List.iter ~f:(fun ((_, rect) as node) -> if intersects_point rect point then f node) elts;
      match categorise_point tree_box point with
      | `SE -> find_all_iter_tree f (se tree_box) point se'
      | `NE -> find_all_iter_tree f (ne tree_box) point ne'
      | `SW -> find_all_iter_tree f (sw tree_box) point sw'
      | `NW -> find_all_iter_tree f (nw tree_box) point nw'

  let rec iter_all f = function
    | Leaf elts -> List.iter ~f elts
    | Quad {nw;ne;sw;se;elts} ->
      List.iter ~f elts;
      iter_all f nw;
      iter_all f ne;
      iter_all f sw;
      iter_all f se

  let iter ?point {dim;tree} ~f =
    match point with
    | Some point ->
      let tree_box = (0.,0.), dim in
      find_all_iter_tree f tree_box point tree
    | None ->
      iter_all f tree 

  let rec find_all_contains_tree acc tree_box quad = function
    | Leaf elts ->
      filter_mappend ~f:(fun (node, rect) -> if contains quad rect then Some node else None) elts acc
    | Quad ({nw=nw';ne=ne';sw=sw';se=se';elts}) ->
      let acc = filter_mappend ~f:(fun (node, rect) -> if contains quad rect then Some node else None) elts acc in
      match categorise tree_box quad with
      | `Overlaps ->
        let acc = find_all_contains_tree acc (se tree_box) quad se' in
        let acc = find_all_contains_tree acc (ne tree_box) quad ne' in
        let acc = find_all_contains_tree acc (sw tree_box) quad sw' in
        let acc = find_all_contains_tree acc (nw tree_box) quad nw' in
        acc
      | `SE -> find_all_contains_tree acc (se tree_box) quad se'
      | `NE -> find_all_contains_tree acc (ne tree_box) quad ne'
      | `SW -> find_all_contains_tree acc (sw tree_box) quad sw'
      | `NW -> find_all_contains_tree acc (nw tree_box) quad nw'

  let find_all_contains {dim;tree} quad =
    let tree_box = (0.,0.), dim in
    find_all_contains_tree [] tree_box quad tree

  let rec iter_contains_tree f tree_box quad = function
    | Leaf elts ->
      List.iter ~f:(fun ((_, rect) as node) -> if contains quad rect then f node) elts
    | Quad ({nw=nw';ne=ne';sw=sw';se=se';elts}) ->
      List.iter ~f:(fun ((_, rect) as node) -> if contains quad rect then f node) elts;
      match categorise tree_box quad with
      | `Overlaps ->
        iter_contains_tree f (se tree_box) quad se';
        iter_contains_tree f (ne tree_box) quad ne';
        iter_contains_tree f (sw tree_box) quad sw';
        iter_contains_tree f (nw tree_box) quad nw'
      | `SE -> iter_contains_tree f (se tree_box) quad se'
      | `NE -> iter_contains_tree f (ne tree_box) quad ne'
      | `SW -> iter_contains_tree f (sw tree_box) quad sw'
      | `NW -> iter_contains_tree f (nw tree_box) quad nw'

  let iter_contains {dim;tree} quad ~f =
    let tree_box = (0.,0.), dim in
    iter_contains_tree f tree_box quad tree

  let rec find_all_intersects_tree acc tree_box quad = function
    | Leaf elts ->
      filter_mappend ~f:(fun (node, rect) -> if intersects quad rect then Some node else None) elts acc
    | Quad ({nw=nw';ne=ne';sw=sw';se=se';elts}) ->
      let acc = filter_mappend ~f:(fun (node, rect) -> if intersects quad rect then Some node else None) elts acc in
      match categorise tree_box quad with
      | `Overlaps ->
        let acc = find_all_intersects_tree acc (se tree_box) quad se' in
        let acc = find_all_intersects_tree acc (ne tree_box) quad ne' in
        let acc = find_all_intersects_tree acc (sw tree_box) quad sw' in
        let acc = find_all_intersects_tree acc (nw tree_box) quad nw' in
        acc
      | `SE -> find_all_intersects_tree acc (se tree_box) quad se'
      | `NE -> find_all_intersects_tree acc (ne tree_box) quad ne'
      | `SW -> find_all_intersects_tree acc (sw tree_box) quad sw'
      | `NW -> find_all_intersects_tree acc (nw tree_box) quad nw'

  let find_all_intersects {dim;tree} quad =
    let tree_box = (0.,0.), dim in
    find_all_intersects_tree [] tree_box quad tree

  let rec iter_intersects_tree f tree_box quad = function
    | Leaf elts ->
      List.iter ~f:(fun ((_, rect) as node) -> if intersects quad rect then f node) elts
    | Quad ({nw=nw';ne=ne';sw=sw';se=se';elts}) ->
      List.iter ~f:(fun ((_, rect) as node) -> if intersects quad rect then f node) elts;
      match categorise tree_box quad with
      | `Overlaps ->
        iter_intersects_tree f (se tree_box) quad se';
        iter_intersects_tree f (ne tree_box) quad ne';
        iter_intersects_tree f (sw tree_box) quad sw';
        iter_intersects_tree f (nw tree_box) quad nw'
      | `SE -> iter_intersects_tree f (se tree_box) quad se'
      | `NE -> iter_intersects_tree f (ne tree_box) quad ne'
      | `SW -> iter_intersects_tree f (sw tree_box) quad sw'
      | `NW -> iter_intersects_tree f (nw tree_box) quad nw'

  let iter_intersects {dim;tree} quad ~f =
    let tree_box = (0.,0.), dim in
    iter_intersects_tree f tree_box quad tree

  let rec find_tree f acc tree_box point =
    let find_elts f point elts =
      List.find ~f:(fun (node, rect) -> intersects_point rect point && f (node, rect)) elts in
    function
    | Leaf elts -> find_elts f point elts
    | Quad ({nw=nw';ne=ne';sw=sw';se=se';elts}) ->
      match (find_elts f point elts) with
      | Some _ as result -> result
      | None ->
        match categorise_point tree_box point with
        | `SE -> find_tree f acc (se tree_box) point se'
        | `NE -> find_tree f acc (ne tree_box) point ne'
        | `SW -> find_tree f acc (sw tree_box) point sw'
        | `NW -> find_tree f acc (nw tree_box) point nw'

  let rec find_tree_nopoint f acc tree_box =
    let find_elts f elts =
      List.find ~f:(fun (node, rect) -> f (node, rect)) elts in
    function
    | Leaf elts -> find_elts f elts
    | Quad ({nw=nw';ne=ne';sw=sw';se=se';elts}) ->
      match (find_elts f elts) with
      | Some _ as result -> result
      | None ->
        let or_else f = function None -> f () | Some _ as result -> result in
        find_tree_nopoint f acc (se tree_box) se'
        |> or_else (fun () -> find_tree_nopoint f acc (ne tree_box) ne')
        |> or_else (fun () -> find_tree_nopoint f acc (sw tree_box) sw')
        |> or_else (fun () -> find_tree_nopoint f acc (nw tree_box) nw')

  let find ?point {dim;tree}  ~f =
    let tree_box = (0.,0.), dim in
    match point with
    | None -> find_tree_nopoint f [] tree_box tree
    | Some point -> find_tree f [] tree_box point tree

  let rec find_map_tree f acc tree_box point =
    let find_elts f point elts =
      List.find_map ~f:(fun (node, rect) ->
        if intersects_point rect point
        then f (node, rect) 
        else None
      ) elts in
    function
    | Leaf elts -> find_elts f point elts
    | Quad ({nw=nw';ne=ne';sw=sw';se=se';elts}) ->
      match (find_elts f point elts) with
      | Some _ as result -> result
      | None ->
        match categorise_point tree_box point with
        | `SE -> find_map_tree f acc (se tree_box) point se'
        | `NE -> find_map_tree f acc (ne tree_box) point ne'
        | `SW -> find_map_tree f acc (sw tree_box) point sw'
        | `NW -> find_map_tree f acc (nw tree_box) point nw'

  let rec find_map_tree_nopoint f acc tree_box =
    let find_elts f elts =
      List.find_map ~f:(fun (node, rect) ->
        f (node, rect) 
      ) elts in
    function
    | Leaf elts -> find_elts f elts
    | Quad ({nw=nw';ne=ne';sw=sw';se=se';elts}) ->
      match (find_elts f elts) with
      | Some _ as result -> result
      | None ->
        let or_else f = function None -> f () | Some _ as result -> result in
        find_map_tree_nopoint f acc (se tree_box) se'
        |> or_else (fun () -> find_map_tree_nopoint f acc (ne tree_box) ne')
        |> or_else (fun () -> find_map_tree_nopoint f acc (sw tree_box) sw')
        |> or_else (fun () -> find_map_tree_nopoint f acc (nw tree_box) nw')

  let find_map ?point {dim;tree} ~f =
    let tree_box = (0.,0.), dim in
    match point with
    | None -> find_map_tree_nopoint f [] tree_box tree
    | Some point -> find_map_tree f [] tree_box point tree

  let rec map_tree f tree_box point =
    let filter_elts f point elts =
      List.map elts ~f:(fun (value,bbox) ->
        if intersects_point bbox point
        then (f (value, bbox), bbox)
        else (value,bbox)
      ) in
    function
    | Leaf elts ->
      Leaf (filter_elts f point elts)
    | Quad ({nw=nw';ne=ne';sw=sw';se=se';elts} as quad) ->
      let elts = filter_elts f point elts in
      match categorise_point tree_box point with
      | `SE -> Quad ({quad with se=map_tree f (se tree_box) point se'; elts})
      | `NE -> Quad ({quad with ne=map_tree f (ne tree_box) point ne'; elts})
      | `SW -> Quad ({quad with sw=map_tree f (sw tree_box) point sw'; elts})
      | `NW -> Quad ({quad with nw=map_tree f (nw tree_box) point nw'; elts})

  let rec map_tree_nopoint f tree_box =
    let filter_elts f elts =
      List.map elts ~f:(fun (value,bbox) -> (f (value, bbox), bbox)) in
    function
    | Leaf elts ->
      Leaf (filter_elts f elts)
    | Quad ({nw=nw';ne=ne';sw=sw';se=se';elts}) ->
      let elts = filter_elts f elts in
      let se=map_tree_nopoint f (se tree_box) se' in
      let ne=map_tree_nopoint f (ne tree_box) ne' in
      let sw=map_tree_nopoint f (sw tree_box) sw' in
      let nw=map_tree_nopoint f (nw tree_box) nw' in
      Quad {nw;ne;sw;se;elts}

  let map ?point {dim;tree} ~f =
    let tree_box = (0.,0.), dim in
    match point with
    | None ->
      let tree = map_tree_nopoint f tree_box tree in
      {dim;tree}
    | Some point ->
      let tree = map_tree f tree_box point tree in
      {dim;tree}


  let rec filter_tree f tree_box point =
    let filter_elts f point elts =
      List.filter elts ~f:(fun (value,bbox) ->
        if intersects_point bbox point
        then f (value,bbox)
        else true) in
    function
    | Leaf elts ->
      Leaf (filter_elts f point elts)
    | Quad ({nw=nw';ne=ne';sw=sw';se=se';elts} as quad) ->
      let elts = filter_elts f point elts in
      match categorise_point tree_box point with
      | `SE -> Quad ({quad with se=filter_tree f (se tree_box) point se'; elts})
      | `NE -> Quad ({quad with ne=filter_tree f (ne tree_box) point ne'; elts})
      | `SW -> Quad ({quad with sw=filter_tree f (sw tree_box) point sw'; elts})
      | `NW -> Quad ({quad with nw=filter_tree f (nw tree_box) point nw'; elts})

  let rec filter_tree_nopoint f tree_box =
    let filter_elts f elts =
      List.filter elts ~f:(fun (value,bbox) ->
        f (value,bbox)
      ) in
    function
    | Leaf elts ->
      Leaf (filter_elts f elts)
    | Quad ({nw=nw';ne=ne';sw=sw';se=se';elts}) ->
      let elts = filter_elts f elts in
      let se=filter_tree_nopoint f (se tree_box) se' in
      let ne=filter_tree_nopoint f (ne tree_box) ne' in
      let sw=filter_tree_nopoint f (sw tree_box) sw' in
      let nw=filter_tree_nopoint f (nw tree_box) nw' in
      Quad {nw;ne;sw;se;elts}

  let filter ?point {dim;tree} ~f =
    let tree_box = (0.,0.), dim in
    match point with
    | None ->
      let tree = filter_tree_nopoint f tree_box tree in
      {dim;tree}
    | Some point ->
      let tree = filter_tree f tree_box point tree in
      {dim;tree}

  let rec filter_map_tree f tree_box point =
    let filter_elts f point elts =
      List.filter_map elts ~f:(fun (value,bbox) ->
        if intersects_point bbox point then
          match f (value, bbox) with
          | None -> None
          | Some value -> Some (value, bbox)
        else Some (value,bbox)
      ) in
    function
    | Leaf elts ->
      Leaf (filter_elts f point elts)
    | Quad ({nw=nw';ne=ne';sw=sw';se=se';elts} as quad) ->
      let elts = filter_elts f point elts in
      match categorise_point tree_box point with
      | `SE -> Quad ({quad with se=filter_map_tree f (se tree_box) point se'; elts})
      | `NE -> Quad ({quad with ne=filter_map_tree f (ne tree_box) point ne'; elts})
      | `SW -> Quad ({quad with sw=filter_map_tree f (sw tree_box) point sw'; elts})
      | `NW -> Quad ({quad with nw=filter_map_tree f (nw tree_box) point nw'; elts})

  let rec filter_map_tree_nopoint f tree_box =
    let filter_elts f elts =
      List.filter_map elts ~f:(fun (value,bbox) ->
          match f (value, bbox) with
          | None -> None
          | Some value -> Some (value, bbox)
      ) in
    function
    | Leaf elts ->
      Leaf (filter_elts f elts)
    | Quad ({nw=nw';ne=ne';sw=sw';se=se';elts}) ->
      let elts = filter_elts f elts in
      let se=filter_map_tree_nopoint f (se tree_box) se' in
      let ne=filter_map_tree_nopoint f (ne tree_box) ne' in
      let sw=filter_map_tree_nopoint f (sw tree_box) sw' in
      let nw=filter_map_tree_nopoint f (nw tree_box) nw' in
      Quad {nw;ne;sw;se;elts}

  let filter_map ?point {dim;tree} ~f =
    let tree_box = (0.,0.), dim in
    match point with
    | None ->
      let tree = filter_map_tree_nopoint f tree_box tree in
      {dim;tree}
    | Some point ->
      let tree = filter_map_tree f tree_box point tree in
      {dim;tree}

  let rec remove_first_tree f tree_box point =
    let rem_first_elts f point elts =
      remove_first elts ~f:(fun (value,bbox) ->
        if intersects_point bbox point
        then f (value,bbox)
        else false) in
    let (let+) x f = Option.bind x ~f in
    function
    | Leaf elts ->
      let+ (elts,result) = rem_first_elts f point elts in
      Some (Leaf elts, result)
    | Quad ({nw=nw';ne=ne';sw=sw';se=se';elts} as quad) ->
      match rem_first_elts f point elts with
      | Some (elts, result) -> Some (Quad {quad with elts}, result)
      | None ->
        match categorise_point tree_box point with
        | `SE ->
          let+ se, result = remove_first_tree f (se tree_box) point se' in
          Some (Quad ({quad with se}), result)
        | `NE ->
          let+ ne, result = remove_first_tree f (ne tree_box) point ne' in
          Some (Quad {quad with ne}, result)
        | `SW ->
          let+ sw, result = remove_first_tree f (sw tree_box) point sw' in
          Some (Quad {quad with sw}, result)
        | `NW ->
          let+ nw, result =remove_first_tree f (nw tree_box) point nw' in
          Some (Quad {quad with nw}, result)

  let rec remove_first_tree_nopoint f tree_box =
    let rem_first_elts f elts =
      remove_first elts ~f:(fun (value,bbox) ->
        f (value,bbox)) in
    let (let+) x f = Option.bind x ~f in
    function
    | Leaf elts ->
      let+ (elts,result) = rem_first_elts f elts in
      Some (Leaf elts, result)
    | Quad ({nw=nw';ne=ne';sw=sw';se=se';elts} as quad) ->
      match rem_first_elts f elts with
      | Some (elts, result) -> Some (Quad {quad with elts}, result)
      | None ->
        let or_else f = function None -> f () | Some _ as result -> result in
        (let+ se, result = remove_first_tree_nopoint f (se tree_box) se' in
         Some (Quad ({quad with se}), result))
        |> or_else
             (fun () -> let+ ne, result = remove_first_tree_nopoint f (ne tree_box) ne' in
               Some (Quad {quad with ne}, result))
        |> or_else
             (fun () -> let+ sw, result = remove_first_tree_nopoint f (sw tree_box) sw' in
               Some (Quad {quad with sw}, result))
        |> or_else
             (fun () -> let+ nw, result =remove_first_tree_nopoint f (nw tree_box) nw' in
               Some (Quad {quad with nw}, result))

  let remove_first ?point {dim;tree} ~f =
    let tree_box = (0.,0.), dim in
    match point with
    | None ->
      begin match remove_first_tree_nopoint f tree_box tree with
      | None -> None
      | Some (tree, result) -> Some ({dim;tree}, result)
      end
    | Some point ->
      match remove_first_tree f tree_box point tree with
      | None -> None
      | Some (tree, result) -> Some ({dim;tree}, result)


  let rec remove_first_map_tree f tree_box point =
    let rem_first_elts f point elts =
      remove_first_map elts ~f:(fun (value,bbox) ->
        if intersects_point bbox point
        then f (value,bbox)
        else None) in
    let (let+) x f = Option.bind x ~f in
    function
    | Leaf elts ->
      let+ (elts,result) = rem_first_elts f point elts in
      Some (Leaf elts, result)
    | Quad ({nw=nw';ne=ne';sw=sw';se=se';elts} as quad) ->
      match rem_first_elts f point elts with
      | Some (elts, result) -> Some (Quad {quad with elts}, result)
      | None ->
        match categorise_point tree_box point with
        | `SE ->
          let+ se, result = remove_first_map_tree f (se tree_box) point se' in
          Some (Quad ({quad with se}), result)
        | `NE ->
          let+ ne, result = remove_first_map_tree f (ne tree_box) point ne' in
          Some (Quad {quad with ne}, result)
        | `SW ->
          let+ sw, result = remove_first_map_tree f (sw tree_box) point sw' in
          Some (Quad {quad with sw}, result)
        | `NW ->
          let+ nw, result =remove_first_map_tree f (nw tree_box) point nw' in
          Some (Quad {quad with nw}, result)

  let rec remove_first_map_tree_nopoint f tree_box =
    let rem_first_elts f elts =
      remove_first_map elts ~f:(fun (value,bbox) ->
        f (value,bbox)) in
    let (let+) x f = Option.bind x ~f in
    function
    | Leaf elts ->
      let+ (elts,result) = rem_first_elts f elts in
      Some (Leaf elts, result)
    | Quad ({nw=nw';ne=ne';sw=sw';se=se';elts} as quad) ->
      match rem_first_elts f elts with
      | Some (elts, result) -> Some (Quad {quad with elts}, result)
      | None ->
        let or_else f = function None -> f () | Some _ as result -> result in
        (let+ se, result = remove_first_map_tree_nopoint f (se tree_box) se' in
         Some (Quad ({quad with se}), result))
        |> or_else
             (fun () -> let+ ne, result = remove_first_map_tree_nopoint f (ne tree_box) ne' in
               Some (Quad {quad with ne}, result))
        |> or_else
             (fun () -> let+ sw, result = remove_first_map_tree_nopoint f (sw tree_box) sw' in
               Some (Quad {quad with sw}, result))
        |> or_else
             (fun () -> let+ nw, result =remove_first_map_tree_nopoint f (nw tree_box) nw' in
               Some (Quad {quad with nw}, result))

  let remove_first_map ?point {dim;tree} ~f =
    let tree_box = (0.,0.), dim in
    match point with
    | None ->
      begin match remove_first_map_tree_nopoint f tree_box tree with
      | None -> None
      | Some (tree, result) -> Some ({dim;tree}, result)
      end
    | Some point ->
      match remove_first_map_tree f tree_box point tree with
      | None -> None
      | Some (tree, result) -> Some ({dim;tree}, result)

  let update ?point qtree ~(f:(('a * rect) -> ('b* rect) option)) =
    let acc = ref [] in
    let qtree = 
      filter_map qtree ~f:(fun ((node, _) as elt) ->
        match f elt with
        | None -> Some node
        | Some result -> acc := result :: !acc; None
      ) ?point in
    List.fold_left ~init:qtree ~f:(fun qtree (node, bbox) ->
      add (node, bbox) qtree
    ) !acc

  let update_first ?point qtree ~(f:(('a * rect) -> ('b* rect) option))  =
    match remove_first_map ?point qtree ~f with
    | Some (qtree, result) ->
      Some (add result qtree)
    | None -> None

end
