open Core

type ('key, 'value) t

val create :
  ?on_delete:('data -> unit) ->
  ?max_size:int -> 'key Hashtbl_intf.Hashtbl.Key.t -> ('key, 'data) t

val capacity : ('a, 'b) t -> int
val update_max_size : ('a, 'b) t -> int -> unit

val add : ('a, 'b) t -> key:'a -> data:'b -> [> `Duplicate | `Ok ]
val add_exn : ('a, 'b) t -> key:'a -> data:'b -> unit
val find : ('a, 'b) t -> 'a -> 'b option
val find_exn : ('a, 'b) t -> 'a -> 'b
val update : ('a, 'b) t -> 'a -> f:('b option -> 'b option) -> unit
