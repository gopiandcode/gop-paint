
type z_index = int
type id

module type COMPONENT = sig
  type t
  val on_mouse_motion : t -> Quadtree.point -> unit
  val on_mouse_down : t -> Quadtree.point -> unit
  val on_mouse_click : t -> Quadtree.point -> unit
  val on_drag : t -> Quadtree.point -> Quadtree.point -> unit
  val on_drag_complete : t -> Quadtree.point -> Quadtree.point -> unit
  val on_draw : t -> unit
end
type 'a component = (module COMPONENT with type t = 'a)

type !'a t

val create : 'a component -> (id -> ('a * z_index) * Quadtree.rect) -> 'a t
val add_component : 'a t -> (id -> ('a * z_index) * Quadtree.rect) -> id
val on_mouse_motion : 'a t -> int * int -> unit
val on_mouse_down : 'a t -> int * int -> unit
val on_mouse_up : 'a t -> int * int -> unit
val on_draw : 'a t -> unit
