open Core
open Tsdl
open Common

type layer_fingerprint = string * int * State.layer [@@deriving sexp, compare, hash]

type t = {
  cache: (layer_fingerprint, Sdl.texture) Lru_cache.t;
  mutable selected_drawing: int;
}



let init (_state: State.t) =
  let on_delete texture =
    Sdl.destroy_texture texture in
  let cache = Lru_cache.create ~on_delete ~max_size:30 (module struct
                       type t = layer_fingerprint [@@deriving sexp, compare, hash]
                     end) in
  {cache; selected_drawing=0}

let lookup_color scene color =
  Array.find_map (snd scene.State.palette) ~f:(fun (_, Color (name, color_value)) ->
    if String.equal color name
    then Some color_value
    else None
  ) |> Option.value ~default:Constants.background_color

let on_draw self renderer (scene: State.t) =
  self.selected_drawing <- min self.selected_drawing (List.length scene.drawings - 1);
  match self.selected_drawing < List.length scene.drawings with
  | (false as _none_selected) -> ()
  | (true  as _some_selected) ->
    let _, ((_, drawing_name), (_, drawing)) = List.nth_exn scene.drawings self.selected_drawing in
    let w, h = drawing.dimensions in
    List.iteri drawing.layers ~f:(fun ind (_, layer) ->
      let draw_sprite renderer sprite =
        !!(Sdl.render_copy renderer sprite) in
      let populate_sprite _renderer sprite =
        let array = Bigarray.(Array1.create int32 c_layout (w * h)) in
        let (.@[]<-) array (i,j) vl = Bigarray.Array1.set array (j * w + i) vl in
        for i = 0 to w - 1 do
          for j = 0 to h - 1 do
            array.@[i,j] <- (Color.(to_int32 @@ of_string "0x00000000"))
          done
        done;
        let Layer {data;_} = layer in
        List.iter data ~f:(function
          | State.Point (color, i, j) ->
            array.@[i,j] <- (Color.to_int32 (lookup_color scene color));
          | State.Points (color, points) ->
            let color = Color.to_int32 @@ lookup_color scene color in
            List.iter points ~f:(fun (i,j) ->
              array.@[i,j] <- color;
            )
        );
        
        !! (Sdl.update_texture sprite None array w);
      in
      Lru_cache.update self.cache (drawing_name, ind, layer) ~f:(function
        | None ->
          let sprite = !! (Sdl.create_texture renderer Sdl.Pixel.format_rgba8888 Sdl.Texture.access_static ~w ~h) in
          !! (Sdl.set_texture_blend_mode sprite Sdl.Blend.mode_blend);
          populate_sprite renderer sprite;
          draw_sprite renderer sprite;
          Some sprite
        | Some sprite ->
          draw_sprite renderer sprite;
          None
      );
    )    
