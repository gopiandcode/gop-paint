open Core

module IntMap = Map.Make(Int)

type ('key, 'value) t = {
  lru_queue: 'key Doubly_linked.t;
  cache: ('key, 'key Doubly_linked.Elt.t * 'value) Hashtbl.t;
  on_delete: 'value -> unit;
  mutable max_size: int;
}

let create ?(on_delete=fun _ -> ()) ?(max_size=10) modl =
  assert (max_size > 0);
  {
    lru_queue=Doubly_linked.create ();
    cache = Hashtbl.create modl;
    max_size; on_delete;
  }

let capacity map = map.max_size - Doubly_linked.length map.lru_queue 

let remove_oldest_element map =
  match Doubly_linked.remove_first map.lru_queue with
  | None -> ()
  | Some key -> Hashtbl.remove map.cache key

let remove_excess_elements map = 
  while capacity map < 0 do
    remove_oldest_element map
  done

let enforce_free_space map =
  remove_excess_elements map;
  if Int.(capacity map = 0)
  then remove_oldest_element map

let update_max_size map max_size =
  map.max_size <- max_size;
  remove_excess_elements map

let add map ~key ~data =
  enforce_free_space map;
  let found_duplicate = ref false in
  Hashtbl.update map.cache key ~f:(function
    | None ->
      let elt = Doubly_linked.insert_last map.lru_queue key in
      (elt, data)
    | Some (elt, old_data) ->
      found_duplicate := true;
      map.on_delete old_data;
      Doubly_linked.move_to_back map.lru_queue elt;
      (elt,data)
  );
  if !found_duplicate
  then `Ok
  else `Duplicate

let add_exn map ~key ~data =
  enforce_free_space map;
  assert (not (Hashtbl.mem map.cache key));
  let elt = Doubly_linked.insert_last map.lru_queue key in
  Hashtbl.add_exn map.cache ~key ~data:(elt, data)

let find map key =
  Hashtbl.find map.cache key
  |> Option.map ~f:(fun (elt, data) ->
    Doubly_linked.move_to_back map.lru_queue elt;
    data
  )

let find_exn map key =
  let (elt,data) = Hashtbl.find_exn map.cache key in
  Doubly_linked.move_to_back map.lru_queue elt;
  data

let update map key ~f =
  enforce_free_space map;
  Hashtbl.change map.cache key ~f:(function
    | None ->
      begin match f None with
      | None -> None
      | Some result ->
        let elt = Doubly_linked.insert_last map.lru_queue key in
        Some (elt, result)
      end
    | Some (elt, old_data) ->
      Doubly_linked.move_to_back map.lru_queue elt;
      begin match f (Some old_data) with
      | None -> None
      | Some data ->
        map.on_delete old_data;
        Some (elt,data)
      end
  )
