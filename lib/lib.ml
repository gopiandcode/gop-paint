[@@@warning "-26"]
open Tsdl
open Core
open Common

(* ** File loading *)

let load_sexp_from_file in_file =
  try Some (Parser.parse in_file) with _ -> None

let load_state_from_sexp sexp =
  Printf.printf "%s\n%!" (Sexp.to_string_hum @@ Sexplib.Sexp_with_layout.Forget.t sexp);
  try Some (State.from_sexp @@ sexp) with err ->
    eprintf "%s\n" (Exn.to_string err);
    None

let load_state_from_file in_file =
  Option.bind (load_sexp_from_file in_file) ~f:(fun sexp -> load_state_from_sexp sexp)

let open_file filename =
  try
    Unix.openfile ~mode:Unix.[O_RDWR; O_CREAT] filename
  with
  | Unix.Unix_error (_) -> failwith @@ "Could not open file \"" ^ filename ^ "\"."

(* ** Game loop *)
let main filename =
  let in_file = open_file filename in
  let last_modification_time = ref (Unix.fstat in_file).Unix.st_mtime in
  let state = load_state_from_file in_file |> function
  | None -> failwith "Could not build initial state as input file had a syntax error."
  | Some s -> ref s in
  let update_state_if_changes () =
    let modification_time = (Unix.fstat in_file).Unix.st_mtime in
    if Float.(modification_time > !last_modification_time) then begin
      last_modification_time := modification_time;
      Option.iter (load_state_from_file in_file) ~f:(fun s ->
        state := s
      )
    end in

  let config = State.config !state in
  let flags = match config.State.fullscreen with
    | true -> Sdl.Window.(fullscreen + hidden)
    | false -> Sdl.Window.(windowed + hidden) in
  let window = !! (Sdl.create_window ~w:config.State.width ~h:config.State.height "GopPaint" flags) in
  let renderer = !! (Sdl.create_renderer ~flags:Sdl.Renderer.accelerated window) in

  let palette = Palette.init !state in
  let canvas = Canvas.init !state in

  let module Component = struct
    type t =
      | Palette
      | Canvas

    let on_mouse_motion : t -> Quadtree.point -> unit =
      fun _ _ -> ()

    let on_mouse_down : t -> Quadtree.point -> unit =
      fun _ _ -> ()

    let on_mouse_click : t -> Quadtree.point -> unit =
      fun item pos -> match item with
        | Palette  -> Palette.on_mouse_click palette pos
        | Canvas -> ()

    let on_drag : t -> Quadtree.point -> Quadtree.point -> unit =
      fun _ _ _ -> ()    

    let on_drag_complete : t -> Quadtree.point -> Quadtree.point -> unit =
      fun _ _ _ -> ()

    let on_draw : t -> unit = function
      | Palette -> Palette.on_draw palette renderer !state
      | Canvas -> Canvas.on_draw canvas renderer !state
  end in

  let ui_manager =
    Ui_controller.create (module Component)
      (fun _ -> ((Component.Canvas, 0),
                 ((0.,0.), {w=Float.of_int config.width;h=Float.of_int config.height}))) in

  ignore @@ Ui_controller.add_component ui_manager
    (fun _ ->
       let py = config.State.height - Constants.palette_height in
       let px = ((config.State.width - Constants.palette_width) / 2) in
       ((Component.Palette, 1),
        ((Float.of_int px, Float.of_int  py),
         {w=Float.of_int Constants.palette_width; h=Float.of_int Constants.palette_height})
       )
    );

  (* state |> State.show |> print_endline; *)

  !! (Sdl.init Sdl.Init.(video + events));


  Sdl.show_window window;

  let event = Sdl.Event.create () in
  let should_quit = ref false in

  let green = Color.of_string "0x22a333" in
  let red = Color.of_string "0xa32233" in
  let rect = Sdl.Rect.create ~x:0 ~y:0 ~w:0 ~h:0 in

  let last_draw_call_time = ref (Sdl.get_ticks ()) in
  let last_update_time = ref (Sdl.get_ticks ()) in
  while not !should_quit do
    let current_time = Sdl.get_ticks () in
    let time_since_last_draw = Int32.(current_time - !last_draw_call_time) in
    let remaining_time_in_frame = Int32.(Constants.ticks_per_frame - time_since_last_draw) in

    if Int32.(remaining_time_in_frame <= 0l) then begin
      last_draw_call_time := current_time;
      (* clear screen *)
      renderer #@ Sdl.set_render_draw_color Constants.background_color;
      !! (Sdl.render_clear renderer);

      Ui_controller.on_draw ui_manager;

      (* draw call *)
      Sdl.render_present renderer;
    end;

    (* handle events *)
    while Sdl.poll_event (Some event) do
      begin match Sdl.Event.(enum @@ get event typ) with
      | `Quit -> should_quit := true
      | `Key_down when Sdl.K.q = Sdl.Event.(get event keyboard_keycode) -> should_quit := true
      | `Mouse_motion ->
        let x = Sdl.Event.(get event mouse_motion_x)
        and y = Sdl.Event.(get event mouse_motion_y) in
        Ui_controller.on_mouse_motion ui_manager (x,y);
      | `Mouse_button_down ->
        let x = Sdl.Event.(get event mouse_button_x)
        and y = Sdl.Event.(get event mouse_button_y) in
        Ui_controller.on_mouse_down ui_manager (x,y);
      | `Mouse_button_up ->
        let x = Sdl.Event.(get event mouse_button_x)
        and y = Sdl.Event.(get event mouse_button_y) in
        Ui_controller.on_mouse_up ui_manager (x,y);
      | ev -> print_endline @@ "\t" ^ Utils.sdl_enum_to_string ev
      end;
    done;
    if Int32.(rem current_time 1000l = 0l) then
      update_state_if_changes ();
  done;
  Sdl.destroy_renderer renderer;
  Sdl.destroy_window window;
  Sdl.quit ()
