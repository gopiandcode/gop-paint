

let (!!) = function
  | Ok result -> result
  | Error (`Msg msg) -> failwith @@ "SDL Error \"" ^ msg ^ "\""


let (#@) renderer fn color = !! Color.(fn renderer color.r color.g color.b color.a)
