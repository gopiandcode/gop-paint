open Core

type z_index = int
type id = int
module IntMap = Map.Make (Int)

type state =
  | Hovered of id [@unboxed]
  | MouseDown of id [@unboxed]
  | Drag of float * float * id [@unboxed]

module type COMPONENT = sig
  type t
  val on_mouse_motion: t -> Quadtree.point -> unit
  val on_mouse_down: t -> Quadtree.point -> unit
  val on_mouse_click: t -> Quadtree.point -> unit
  val on_drag: t -> Quadtree.point -> Quadtree.point -> unit
  val on_drag_complete: t -> Quadtree.point -> Quadtree.point -> unit
  val on_draw: t -> unit
end

type 'a component = (module COMPONENT with type t = 'a)

let on_mouse_motion (type a) ((module C): a component) elt point =
  C.on_mouse_motion elt point
let on_mouse_down (type a) ((module C): a component) elt point =
  C.on_mouse_down elt point
let on_mouse_click (type a) ((module C): a component) elt point =
  C.on_mouse_click elt point
let on_drag (type a) ((module C): a component) elt start current =
  C.on_drag elt start current
let on_drag_complete (type a) ((module C): a component) elt start current =
  C.on_drag_complete elt start current
let on_draw (type a) ((module C): a component) elt =
  C.on_draw elt


type !'a t = {
  component: 'a component;
  mutable next_id: id;
  mutable tree: id Quadtree.Fixed.t;
  mutable data: ('a * z_index) IntMap.t;
  mutable draw_map: 'a list IntMap.t;

  root_element: id;

  mutable mouse_x: float; mutable mouse_y: float;

  mutable state: state;
}

let create component (root: id -> ('a * z_index) * Quadtree.rect) =
  let (root, z_index), ((_, dim) as rect) = root 0 in
  assert (Poly.equal ((0., 0.),dim) rect);
  let tree = Quadtree.Fixed.create dim |> Quadtree.Fixed.add (0, rect) in
  let data = IntMap.empty |> IntMap.add_exn ~key:0 ~data:(root, z_index) in
  let draw_map = IntMap.empty |> IntMap.add_exn ~key:z_index ~data:[root] in
  let next_id = 1 in
  { next_id; tree; data; root_element=0; mouse_x = 0.; mouse_y = 0.; state=Hovered 0; component; draw_map }

let add_component it (component: id -> ('a * z_index) * Quadtree.rect) =
  let id = it.next_id in
  let ((elt, z_index) as component), rect = component id in
  it.next_id <- it.next_id + 1;
  it.tree <- Quadtree.Fixed.add (id, rect) it.tree;
  it.data <- IntMap.add_exn ~key:id ~data:component it.data;
  it.draw_map <- IntMap.add_multi it.draw_map ~key:z_index ~data:elt;
  id

let on_mouse_motion it (x,y) =
  let x = Float.of_int x and y = Float.of_int y in
  match it.state with
  | Hovered _ ->
    let active_element = 
      begin match Quadtree.Fixed.find ~point:(x,y) it.tree ~f:(fun _ -> true) with
      | None -> it.root_element
      | Some (id, _) -> id 
      end in
    let element,_ = IntMap.find_exn it.data active_element in
    on_mouse_motion it.component element (x,y);
    it.mouse_x <- x; it.mouse_y <- y;
    it.state <- Hovered active_element
  | MouseDown id ->
    let element, _ = IntMap.find_exn it.data id in
    on_drag it.component element (it.mouse_x, it.mouse_y) (x,y);
    it.mouse_x <- x; it.mouse_y <- y;
    it.state <- Drag (it.mouse_x, it.mouse_y, id)
  | Drag (start_x, start_y, id) -> 
    let element,_ = IntMap.find_exn it.data id in
    it.mouse_x <- x; it.mouse_y <- y;
    on_drag it.component element (start_x, start_y) (x,y)

let on_mouse_down it (x,y) =
  let x = Float.of_int x and y = Float.of_int y in
  it.mouse_x <- x; it.mouse_y <- y;
  match it.state with
  | Hovered id ->
    let element, _ = IntMap.find_exn it.data id in
    it.state <- MouseDown id;
    on_mouse_down it.component element (x,y)
  | MouseDown id ->
    let element, _ = IntMap.find_exn it.data id in
    on_mouse_down it.component element (x,y)
  | Drag (_, _, _) -> 
    let active_element = 
      begin match Quadtree.Fixed.find ~point:(x,y) it.tree ~f:(fun _ -> true) with
      | None -> it.root_element
      | Some (id, _) -> id 
      end in
    let element, _ = IntMap.find_exn it.data active_element in
    on_mouse_down it.component element (x,y);
    it.state <- MouseDown active_element

let on_mouse_up it (x,y) = 
  let x = Float.of_int x and y = Float.of_int y in
  it.mouse_x <- x; it.mouse_y <- y;
  match it.state with
  | Hovered _ -> ()
  | MouseDown id ->
    let element, _ = IntMap.find_exn it.data id in
    on_mouse_click it.component element (x,y);
    it.state <- Hovered it.root_element;
    ()
  | Drag (start_x, start_y, id) ->
    let element, _ = IntMap.find_exn it.data id in
    on_drag_complete it.component element (start_x, start_y) (x,y);
    it.state <- Hovered it.root_element

let on_draw it =
  IntMap.iter it.draw_map ~f:(fun elts ->
    List.iter elts ~f:(on_draw it.component)
  )
