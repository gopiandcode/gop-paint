open Core
open Position
open Sexp_utils

type color = Color of string * Color.t
 [@@deriving isexp, show { with_path = false }]

type palette = color with_pos array
 [@@deriving isexp, show { with_path = false }]

type draw_element =
  | Point of string * int * int
  | Points of string * (int * int) list
[@@deriving isexp, sexp, hash, compare, show { with_path = false }]

type config = {
  width: int;
  height: int;
  fullscreen: bool [@default false]
}  [@@deriving isexp, show { with_path = false }]

let default_config = {width=1280; height=720; fullscreen=false}

type layer = Layer of {
  frame: int option;
  opacity: float option;
  data: draw_element list;
} [@@deriving isexp, sexp, hash, compare, show { with_path = false }]

type drawing = {
  dimensions: int * int [@default (32, 32)];
  layers: layer with_pos list;
} [@@deriving isexp, show { with_path = false }]

type t = {
  config: config option;
  palette: palette with_pos;
  drawings: (string with_pos * drawing with_pos) with_pos list;
} [@@deriving isexp, show { with_path = false }]

let config t = Option.value ~default:default_config t.config 
