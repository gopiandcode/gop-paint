open Core

let ticks_per_frame = Int32.(1000l / 60l)

let palette_width = 820
let palette_height = 100
let palette_swatch_size = 80
let palette_swatch_padding = 10

let background_color = Color.of_rgb (0x10, 0x00, 0x10)
let ui_element_color = Color.of_rgb (0xbb, 0x88, 0xff)
let ui_outline_color = Color.of_rgb (0x88, 0x22, 0xaa)
