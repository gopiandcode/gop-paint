type point = float * float
type dim = {w: float; h: float}
type rect = point * dim

val midpoint: rect -> point
val intersects: rect -> rect -> bool
val contains: rect -> rect -> bool
val points: rect -> point list

module Fixed: sig
  type +!'a t

  val bounding_boxes: 'a t -> rect list

  val create: dim -> 'a t

  val add : 'a * rect -> 'a t -> 'a t

  val of_list: dim -> ('a * rect) list -> 'a t

  val to_list: 'a t -> ('a * rect) list

  val find_all : 'a t -> point -> 'a list

  val iter: ?point:point -> 'a t -> f:('a * rect -> unit) -> unit

  val find_all_contains : 'a t -> rect -> 'a list

  val iter_contains: 'a t -> rect -> f:('a * rect -> unit) -> unit

  val find_all_intersects : 'a t -> rect -> 'a list

  val iter_intersects: 'a t -> rect -> f:('a * rect -> unit) -> unit

  val map : ?point:point -> 'a t -> f:('a * rect -> 'a) -> 'a t

  val find : ?point:point -> 'a t ->  f:('a * rect -> bool) -> ('a * rect) option

  val find_map : ?point:point -> 'a t -> f:('a * rect -> 'b option) -> 'b option

  val filter : ?point:point -> 'a t -> f:('a * rect -> bool) -> 'a t

  val filter_map : ?point:point -> 'a t -> f:('a * rect -> 'a option) -> 'a t

  val remove_first : ?point:point -> 'a t -> f:('a * rect -> bool) -> ('a t * ('a * rect)) option

  val remove_first_map : ?point:point -> 'a t -> f:('a * rect -> 'b option) -> ('a t * 'b) option

  val update : ?point:point -> 'b t -> f:('b * rect -> ('b * rect) option) -> 'b t

  val update_first : ?point:point -> 'b t -> f:('b * rect -> ('b * rect) option) -> 'b t option

end
