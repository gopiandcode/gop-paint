open Core

let rec t_from_sexp f = function
  | Sexplib.Sexp.With_layout.Atom (_, s, _) -> f s
  | List (_, ls, _) ->
    List.filter_map ls ~f:(function Sexplib.Sexp.With_layout.Sexp s -> Some s | _ -> None)
    |> List.hd_exn |> t_from_sexp f

let string_from_sexp = t_from_sexp Fun.id

let int_from_sexp = t_from_sexp Int.of_string

let float_from_sexp = t_from_sexp Float.of_string

let bool_from_sexp = t_from_sexp Bool.of_string
