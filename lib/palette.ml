open Tsdl
open Core
open Common


let max_swatches = Constants.((palette_width - palette_swatch_padding) /
                   (palette_swatch_size + palette_swatch_padding)) 


type t = {
  base_rect: Sdl.rect;
  swatch_rect: Sdl.rect;
  mutable selected: int option;
}


let init state =
  let State.{width;height;_} = State.config state in
  let y = (height - Constants.palette_height) in
  let x = ((width - Constants.palette_width) / 2) in
  let base_rect = Sdl.Rect.create ~x ~y ~w:Constants.palette_width ~h:Constants.palette_height in
  let swatch_rect = Sdl.Rect.create ~x:0 ~y:0 ~w:Constants.palette_swatch_size ~h:Constants.palette_swatch_size in
  {base_rect; swatch_rect; selected=None}

let on_draw self renderer state =
  renderer #@ Sdl.set_render_draw_color Constants.ui_element_color;
  !!(Sdl.render_fill_rect renderer (Some self.base_rect));
  renderer #@ Sdl.set_render_draw_color Constants.ui_outline_color;
  !!(Sdl.render_draw_rect renderer (Some self.base_rect));
  let offset_x = ref (Sdl.Rect.x self.base_rect + Constants.palette_swatch_padding) in
  let offset_y = ref (Sdl.Rect.y self.base_rect + Constants.palette_swatch_padding) in
  let palette = snd state.State.palette in
  for i = 0 to min max_swatches (Array.length palette) - 1 do
    let _, Color (_, color) = palette.(i) in
    Sdl.Rect.set_x self.swatch_rect (!offset_x);
    Sdl.Rect.set_y self.swatch_rect (!offset_y);
    renderer #@ Sdl.set_render_draw_color color;
    !!(Sdl.render_fill_rect renderer (Some self.swatch_rect));
    if Poly.(self.selected = Some i) then begin
      renderer #@ Sdl.set_render_draw_color Constants.ui_outline_color;
      !!(Sdl.render_draw_rect renderer (Some self.swatch_rect));        
    end;
    offset_x := !offset_x + Constants.palette_swatch_size + Constants.palette_swatch_padding;
  done  

let on_mouse_click _self (_x,_y) =
  Format.printf "received click at %f,%f\n%!" _x _y;
  ()
