open Core

let parse input =
  let read buf len = Unix.read ~pos:0 input ~buf ~len in
  let lexer lexbuf = Sexplib.Lexer.main_with_layout lexbuf in
  (* seek to the start *)
  ignore @@ Unix.lseek input 0L ~mode:SEEK_SET;
  let lexbuf = Lexing.from_function ~with_positions:true read in
  let result = Sexplib.Parser_with_layout.sexps lexer lexbuf in
  Sexp.With_layout.(List ({row=0;col=0}, result, {row=0;col=0}))
