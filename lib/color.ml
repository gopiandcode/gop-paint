open Core
type t = {r: int; g: int; b: int; a:int}

let pp fmt = function
  | {r;g;b;a=0xFF} when r = g && g = b ->
    Format.fprintf fmt "0x%02x" r
  | {r;g;b;a} when r = g && g = b ->
    Format.fprintf fmt "0x%02x%02x" r a
  | {r;g;b;a=0xFF} ->
    Format.fprintf fmt "0x%02x%02x%02x" r g b
  | {r;g;b;a} ->
    Format.fprintf fmt "0x%02x%02x%02x%02x" r g b a
let show = Format.asprintf "%a" pp

let int_to_string_hex =
  let rec loop acc i =
    if i > 0
    then loop ((i land 0xFF) :: acc) (i lsr 8)
    else List.map ~f:(fun i -> Format.sprintf "%02x" i) acc  |> String.concat in
  fun i -> "0x" ^ (loop [] i)

let of_rgba (r,g,b,a) = {r;g;b;a}
let of_rgb (r,g,b) = {r;g;b;a=0xff}

let of_string i =
  let vl = Int.of_string i in
  match String.is_prefix ~prefix:"0x" i, String.length i with
  | true, 10 ->
    let r = (vl lsr 24) land 0xFF in
    let g = (vl lsr 16) land 0xFF in
    let b = (vl lsr 8) land 0xFF in
    let a = (vl lsr 0) land 0xFF in
    {r;g;b;a}
  | true, 8 ->
    let r = (vl lsr 16) land 0xFF in
    let g = (vl lsr 8) land 0xFF in
    let b = (vl lsr 0) land 0xFF in
    let a = 0xFF in
    {r;g;b;a}
  | true, 6 ->
    let v = (vl lsr 8) land 0xFF in
    let a = (vl lsr 0) land 0xFF in
    {r=v;g=v;b=v;a}
  | _ ->
    let v = (vl lsr 0) land 0xFF in
    let a = 0xFF in
    {r=v;g=v;b=v;a}

let rec from_sexp: Sexplib.Sexp.With_layout.t -> t =
  let open Sexp.With_layout in
  function
  | Atom (_, str, _) -> of_string str
  | List (_, elts, _) ->
    match List.filter_map ~f:(function Sexp s -> Some s | _ -> None) elts with
    | [Atom (_,r,_);Atom (_,g,_);Atom (_,b,_)] ->
      {r=Int.of_string r land 0xff;
       g=Int.of_string g land 0xff;
       b=Int.of_string b land 0xff;
       a=0xff; }
    | [Atom (_,r,_);Atom (_,g,_);Atom (_,b,_); Atom (_, a, _)] ->
      {r=Int.of_string r land 0xff;
       g=Int.of_string g land 0xff;
       b=Int.of_string b land 0xff;
       a=Int.of_string a land 0xff; }
    | [Atom (_,vl,_);Atom (_,a,_)] ->
      let vl = Int.of_string vl land 0xff in
      {r=vl; g=vl; b=vl; a=Int.of_string a land 0xff; }
    | [Atom (_,vl,_);] -> of_string vl
    | h :: _ -> from_sexp h
    | _ -> failwith "unexpected format of color"

let to_int {r;g;b;a} =
  let vl = 0 in
  let vl = (r lsl 24) lor vl in
  let vl = (g lsl 16) lor vl in
  let vl = (b lsl 8) lor vl in
  let vl = (a lsl 0) lor vl in
  vl

let to_int32 {r;g;b;a} =
  let vl = 0l in
  let vl = Int32.((of_int_trunc r lsl 24) lor vl) in
  let vl = Int32.((of_int_trunc g lsl 16) lor vl) in
  let vl = Int32.((of_int_trunc b lsl 8) lor vl) in
  let vl = Int32.((of_int_trunc a lsl 0) lor vl) in
  vl
