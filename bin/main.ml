open Core

let main filename = function
  | None | Some false ->
    begin fun () ->
      try
        Lib.main filename
      with
      | Failure msg ->
        Printf.exitf "Fatal Error: %s" msg ()
    end
  | Some true -> fun () -> Lib.main filename

let () =
  let filename = Command.Spec.("filename" %: string) in
  let unsafe = Command.Spec.(flag "unsafe" (optional bool) ~doc:"Do not catch errors") in
  let command =
    Command.basic
      ~summary:"Open new project"
      Command.Param.(return main <*> anon filename <*> unsafe) in
  Command.run command
