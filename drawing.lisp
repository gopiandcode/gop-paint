(config
 (width 1080)
 (height 720)
 )

(palette
 (color red 0xaa2050)
 (color light-blue 0x6ccff6)
 (color blue 0x2b59c3)
 (color green 0x00916e)
 (color orange 0xf58f29))

(drawings
 ("main-character"
  ((dimensions 32 32)
   (layers
   (layer
    (frame 1)
    (opacity 80)
    (data
     (points red
             ((0 0) (1 1)
              (2 2) (3 3)
              (4 4) (5 5)
              (6 6) (7 7)
              (8 8) (9 9)))
     (points green
             ((1 0)
              (2 1) (3 2)
              (4 3) (5 4)
              (6 5) (7 6)
              (8 7) (9 8)))     
     (points orange
             ((0 1) (1 2)
              (2 3) (3 4)
              (4 5) (5 6)
              (6 7) (7 8)
              (8 9) (9 10)))
     ))))))

